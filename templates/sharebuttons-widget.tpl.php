<?php
/**
 * @file
 * Sharebuttons implementation to theme the buttons.
 *
 * Sharebuttons variables:
 * $sharebuttons['name'] e.g. $sharebuttons['facebook'].
 * $alignment['class'] e.g. horizontal or vertical.
 */
?>
<div id="sharebuttons-widget">
  <ul class="sharebuttons <?php print $alignment['class']; ?>">
    <?php foreach ($sharebuttons as $name => $button): ?>
      <li class="sharebutton <?php print $name; ?> "><?php print $button; ?></li>
    <?php endforeach; ?>
  </ul>
</div>
