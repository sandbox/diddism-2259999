CONTENTS OF THIS FILE
---------------------
* Introduction
Sharebuttons is a module that provides interactive social media buttons. 
It uses the native HTML5 widget setup as recommended by those social media's.
While providing as much admin-configurable options as they have. 
Needed scripts will be loaded asynchronously (where possible).

Every button is written as a plugin. 
Sharebuttons can be easily extended.
See sharebuttons/plugins/mynetwork.inc.txt for a start.

Buttons can be shown horizontal inline or vertical. 
There are a lot per-button options.
Each button has its own section for setting those options.

* Requirements
There are no extra requirements for this plugin, it works standalone.

* Installation
- Download and extract the sharebuttons-module as usual.
- Enable the module @ /admin/modules.
- Go to admin/config/services/sharebuttons
- Enable at least 1 plugin and nodetype.
- Now you should see the provider-button appear on (full) nodes of that type.

* Configuration
This is pretty self-explanatory I hope. Post an issue if not. 
