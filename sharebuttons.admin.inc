<?php
/**
 * @file
 * Global settings for Sharebuttons module
 */

/**
 * Implements hook_form().
 */
function sharebuttons_settings_form($form, &$form_state) {

  $form['sharebuttons'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sharebuttons global settings'),
  );

  $form['sharebuttons']['sharebuttons_nodes'] = array(
    '#type' => 'checkboxes',
    '#options' => node_type_get_names(),
    '#title' => t('Display in nodes'),
    '#default_value' => variable_get('sharebuttons_nodes', array()),
    '#description' => t('Enable the nodes you want to show Sharebuttons on.'),
  );

  $form['sharebuttons']['sharebuttons_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display in teasers'),
    '#default_value' => variable_get('sharebuttons_teaser', 0),
    '#description' => t('Enable if you want to show Sharebuttons on teasers of the above nodetypes also.'),
  );

  $form['sharebuttons']['sharebuttons_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display in a block'),
    '#default_value' => variable_get('sharebuttons_block', 0),
    '#description' => t('Enable this if you want a block with Sharebuttons. The block-options can be set at <a href="@settings">blocks overview</a>.', array('@settings' => url('admin/structure/block'))),
  );

  $form['sharebuttons']['sharebuttons_weight'] = array(
    '#type' => 'select',
    '#title' => t('Set the weight for the widget'),
    '#options' => range(-50, 50),
    '#default_value' => variable_get('sharebuttons_weight', 60),
    '#description' => t('Set this to sort Sharebuttons in the list of elements (e.g. title, body) in a node; lower numbers display before higher numbers'),
  );

  $form['sharebuttons']['sharebuttons_alignment'] = array(
    '#type' => 'select',
    '#title' => t('Choose how you want to align the buttons'),
    '#options' => array(
      'horizontal' => t('Horizontal'),
      'vertical' => t('Vertical'),
    ),
    '#default_value' => variable_get('sharebuttons_alignment', 'horizontal'),
    '#description' => t('Adds class horizontal or vertical to the Sharebuttons.'),
  );

  $form['sharebuttons']['order'] = array(
    '#type' => 'fieldset',
    '#title' => t('Set the order in which the buttons need to be shown'),
  );

  $plugins = sharebuttons_plugins(); $i = 1;
  foreach ($plugins as $name => $info) {
    $form['sharebuttons']['order']['sharebuttons_' . $name . '_order'] = array(
      '#type' => 'select',
      '#title' => ucfirst($name),
      '#default_value' => variable_get('sharebuttons_' . $name . '_order', $i),
      '#options' => range(1, count($plugins)),
    );
    $i++;
  }

  return system_settings_form($form);

}
