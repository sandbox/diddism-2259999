(function($) {
  Drupal.behaviors.sharebuttons_googleBehavior = {
    attach: function (context, settings) {
      var language = Drupal.settings.sharebuttons_google.lang;
      var action = Drupal.settings.sharebuttons_google.action;

      window.___gcfg = {
        lang: language,
        parsetags: 'onload'
      };

      var po = document.createElement('script');
      po.async = true;
      po.type = 'text/javascript';
      po.src = 'https://apis.google.com/js/' + action + '.js?onload=onLoadCallback';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(po, s);

    }
  };
})(jQuery);
