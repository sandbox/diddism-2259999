(function($, d, s, id) {
  Drupal.behaviors.sharebuttons_facebookBehavior = {
    attach: function (context, settings) {
      var language = Drupal.settings.sharebuttons_facebook.lang;

      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement(s); js.async = true; js.id = id;
      js.src = "//connect.facebook.net/" + language + "/all.js#xfbml=1";
      fjs.parentNode.insertBefore(js, fjs);

    }
  };
})(jQuery, document, 'script', 'facebook-jssdk');
