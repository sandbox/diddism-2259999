<?php
/**
 * @file
 * Implements Pinterest plugin.
 */

/**
 * Pinterest button render.
 */
function sharebuttons_pinterest($url) {

  // Load javascript.
  drupal_add_js(drupal_get_path('module', 'sharebuttons') . '/js/pinterest.js');

  // Get the correct markup.
  $button = 'sharebuttons_pinterest_' . variable_get('sharebuttons_pinterest_type');
  return $button($url);

}


/**
 * Pinterest Pinit! button markup.
 */
function sharebuttons_pinterest_pinit($url) {

  // Get variables.
  $lang = variable_get('sharebuttons_pinterest_language');
  $media = variable_get('sharebuttons_pinterest_media');
  $action = variable_get('sharebuttons_pinterest_media') ? 'Pin' : 'Bookmark';
  $text = check_plain(variable_get('sharebuttons_pinterest_text'));
  $layout = variable_get('sharebuttons_pinterest_layout');
  $counter = variable_get('sharebuttons_pinterest_counter');
  $color = substr($layout, 5, -3);

  // Output button.
  $markup = '<a href="//www.pinterest.com/pin/create/button/';
  $markup .= '?url=' . $url;
  $markup .= '&media=' . urlencode($media);
  $markup .= '&description=' . urlencode($text) . '"';
  $markup .= ' data-pin-do="button' . $action . '"';
  $markup .= ' data-pin-lang="' . $lang . '"';
  $markup .= ' data-pin-height="' . substr($layout, -2) . '"';
  $markup .= ' data-pin-config="' . $counter . '"';
  $markup .= ' data-pin-color="' . $color . '"';
  $markup .= '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_' . $lang . '_' . $layout . '.png" />';
  $markup .= '</a>';
  return $markup;
}


/**
 * Pinterest Follow button markup.
 */
function sharebuttons_pinterest_follow($url) {

  // Get variables.
  $user = check_plain(variable_get('sharebuttons_pinterest_follow_user'));
  $text = check_plain(variable_get('sharebuttons_pinterest_follow_text'));

  // Output button.
  $markup = '<a data-pin-do="buttonFollow"';
  $markup .= 'href="http://www.pinterest.com/' . urlencode($user) . '/"';
  $markup .= '>' . $text . '</a>';
  return $markup;
}


/**
 * Pinterest settings form.
 */
function sharebuttons_pinterest_form($form, &$form_state) {

  $form['pinterest'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pinterest settings'),
  );

  $form['pinterest']['sharebuttons_pinterest_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to enable the Pinterest button'),
    '#default_value' => variable_get('sharebuttons_pinterest_enabled', 0),
  );

  $form['pinterest']['sharebuttons_pinterest_type'] = array(
    '#type' => 'radios',
    '#title' => t('Choose which type of button you want to enable'),
    '#default_value' => variable_get('sharebuttons_pinterest_type', 'pinit'),
    '#required' => TRUE,
    '#options' => array(
      'pinit' => t('Pin it!'),
      'follow' => t('Follow'),
    ),
  );

  $form['pinterest']['pinit'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pin it button'),
  );

  $form['pinterest']['pinit']['sharebuttons_pinterest_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Set this if you want a fixed url'),
    '#description' => t('Leave empty if you want to use the node/page-url'),
    '#attributes' => array('placeholder' => 'https://example.com'),
    '#default_value' => variable_get('sharebuttons_pinterest_url'),
  );

  $form['pinterest']['pinit']['sharebuttons_pinterest_layout'] = array(
    '#type' => 'radios',
    '#title' => t('Select the layout for the button'),
    '#options' => sharebuttons_pinterest_settings('layout'),
    '#default_value' => variable_get('sharebuttons_pinterest_layout', 'rect_gray_20'),
    '#required' => TRUE,
  );

  $form['pinterest']['pinit']['sharebuttons_pinterest_counter'] = array(
    '#type' => 'select',
    '#title' => t('Select the counter for the button'),
    '#options' => sharebuttons_pinterest_settings('counter'),
    '#default_value' => variable_get('sharebuttons_pinterest_counter', 'none'),
    '#required' => TRUE,
  );

  $form['pinterest']['pinit']['sharebuttons_pinterest_media'] = array(
    '#type' => 'textfield',
    '#title' => t('Select a picture you if you want to pin it to the button'),
    '#default_value' => variable_get('sharebuttons_pinterest_media'),
    '#attributes' => array('placeholder' => 'http://example.com/image.png'),
    '#description' => t('Leave empty if you do not want to use a specific image, instead the pin will collect all images on the page for you to pin)'),
  );

  $form['pinterest']['pinit']['sharebuttons_pinterest_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Select a description for the image'),
    '#default_value' => variable_get('sharebuttons_pinterest_text'),
    '#attributes' => array('placeholder' => 'Next stop: Pinterest'),
    '#description' => t('Leave empty if you do not want to display text with an image'),
  );

  $form['pinterest']['pinit']['sharebuttons_pinterest_language'] = array(
    '#type' => 'select',
    '#title' => t('Select the language for the button'),
    '#options' => sharebuttons_pinterest_settings('language'),
    '#default_value' => variable_get('sharebuttons_pinterest_language', 'en'),
    '#required' => TRUE,
  );

  $form['pinterest']['follow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pinterest follow button'),
  );

  $form['pinterest']['follow']['sharebuttons_pinterest_follow_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Pinterest username'),
    '#default_value' => variable_get('sharebuttons_pinterest_follow_user'),
    '#attributes' => array('placeholder' => 'Pinterest username'),
  );

  $form['pinterest']['follow']['sharebuttons_pinterest_follow_text'] = array(
    '#type' => 'textfield',
    '#title' => t('The text for the button'),
    '#default_value' => variable_get('sharebuttons_pinterest_follow_text', 'Follow'),
    '#attributes' => array('placeholder' => t('Follow')),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function sharebuttons_pinterest_form_validate($form, &$form_state) {

  // Validate url.
  if ($form_state['values']['sharebuttons_pinterest_url'] && !valid_url($form_state['values']['sharebuttons_pinterest_url'], TRUE)) {
    form_set_error('sharebuttons_pinterest_url', t('Invalid url: start with http:// or https://.'));
  }
  if ($form_state['values']['sharebuttons_pinterest_media'] && !valid_url($form_state['values']['sharebuttons_pinterest_media'], TRUE)) {
    form_set_error('sharebuttons_pinterest_media', t('Invalid picture url: start with http:// or https://.'));
  }

  // Validate both fields have values if Follow button is enabled.
  if ($form_state['values']['sharebuttons_pinterest_type'] == 'follow') {
    if (!$form_state['values']['sharebuttons_pinterest_follow_user'] || !$form_state['values']['sharebuttons_pinterest_follow_text']) {
      if (!$form_state['values']['sharebuttons_pinterest_follow_user']) {
        form_set_error('sharebuttons_pinterest_follow_user', t('Your Pinterest username is required for the follow button'));
      }
      else {
        form_set_error('sharebuttons_pinterest_follow_text', t('Text is required for the follow button'));
      }
    }
  }

}


/**
 * Pinterest options.
 */
function sharebuttons_pinterest_settings($option) {

  switch ($option) {

    case 'layout':
      $items = array(
        'rect_gray_20' => '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png">',
        'rect_red_20' => '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png">',
        'rect_white_20' => '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_white_20.png">',
        'rect_gray_28' => '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_28.png">',
        'rect_red_28' => '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_28.png">',
        'rect_white_28' => '<img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_white_28.png">',
      );
      return $items;

    case 'counter':
      $items = array(
        'none' => t('No counter'),
        'above' => t('Vertical (counter above of button)'),
        'beside' => t('Horizontal (counter right of button)'),
      );
      return $items;

    case 'language':
      $items = array(
        'en' => t('English'),
        'ja' => t('Japanese'),
      );
      asort($items);
      return $items;

  }
}
