<?php
/**
 * @file
 * Implements Twitter plugin.
 */

/**
 * Twitter button render.
 */
function sharebuttons_twitter($url) {

  // Load javascript.
  drupal_add_js(drupal_get_path('module', 'sharebuttons') . '/js/twitter.js');

  // Get the correct markup.
  $button = 'sharebuttons_twitter_' . variable_get('sharebuttons_twitter_type');
  return $button($url);
}


/**
 * Twitter button Share markup.
 */
function sharebuttons_twitter_share($url) {

  // Get variables.
  $lang = variable_get('sharebuttons_twitter_language');
  $size = variable_get('sharebuttons_twitter_size') ? ' data-size="large"' : '';
  $count = variable_get('sharebuttons_twitter_count') ? 'true' : 'none';
  $via = variable_get('sharebuttons_twitter_via');
  $recommend = variable_get('sharebuttons_twitter_recommend');
  $hashtags = variable_get('sharebuttons_twitter_hashtags');
  $width = variable_get('sharebuttons_twitter_width');
  $tailor = variable_get('sharebuttons_twitter_tailoring') ? 'true' : 'false';

  // Output button.
  $markup = '<a href="https://twitter.com/share" class="twitter-share-button"';
  $markup .= ' data-url="' . $url . '"';
  $markup .= ' data-lang="' . $lang . '"';
  $markup .= $size;
  $markup .= ' data-count="' . $count . '"';
  $markup .= ' data-via="' . check_plain($via) . '"';
  $markup .= ' data-related="' . check_plain($recommend) . '"';
  $markup .= ' data-hashtags="' . check_plain($hashtags) . '"';
  $markup .= ' data-width="' . $width . '"';
  $markup .= ' data-dnt="' . $tailor . '"';
  $markup .= '>Tweet</a>';
  return $markup;
}


/**
 * Twitter button Follow markup.
 */
function sharebuttons_twitter_follow($url) {

  // Get variables.
  $lang = variable_get('sharebuttons_twitter_language');
  $size = variable_get('sharebuttons_twitter_size') ? ' data-size="large"' : '';
  $name = check_plain(variable_get('sharebuttons_twitter_follow_name'));
  $count = variable_get('sharebuttons_twitter_count') ? 'true' : 'false';
  $text = variable_get('sharebuttons_twitter_follow_text') ? 'true' : 'false';
  $tailor = variable_get('sharebuttons_twitter_tailoring') ? 'true' : 'false';

  // Output button.
  $markup = '<a class="twitter-follow-button" href="https://twitter.com/';
  $markup .= urlencode($name) . '"';
  $markup .= ' data-lang="' . $lang . '"';
  $markup .= $size;
  $markup .= ' data-show-count="' . $count . '"';
  $markup .= ' data-show-screen-name="' . $text . '"';
  $markup .= ' data-dnt="' . $tailor . '"';
  $markup .= '>Follow</a>';
  return $markup;
}


/**
 * Twitter button Hashtag markup.
 */
function sharebuttons_twitter_hashtag($url) {

  // Get variables.
  $lang = variable_get('sharebuttons_twitter_language');
  $hashtag = check_plain(variable_get('sharebuttons_twitter_hashtag'));
  $text = check_plain(variable_get('sharebuttons_twitter_hashtag_text'));
  $name = check_plain(variable_get('sharebuttons_twitter_hashtag_recommend'));
  $size = variable_get('sharebuttons_twitter_size') ? ' data-size="large"' : '';
  $count = variable_get('sharebuttons_twitter_count') ? 'true' : 'false';
  $tailor = variable_get('sharebuttons_twitter_tailoring') ? 'true' : 'false';
  $url = variable_get('sharebuttons_twitter_hashtag_url') ? $url : '';

  // Output button.
  $markup = '<a class="twitter-hashtag-button" href="https://twitter.com/intent/tweet?';
  $markup .= 'button_hashtag=' . urlencode($hashtag);
  $markup .= '&text=' . urlencode($text) . '"';
  $markup .= ' data-related="' . $name . '"';
  $markup .= ' data-lang="' . $lang . '"';
  $markup .= $size;
  $markup .= ' data-show-count="' . $count . '"';
  $markup .= ' data-url="' . $url . '"';
  $markup .= ' data-dnt="' . $tailor . '"';
  $markup .= '>#Hashtag</a>';
  return $markup;
}


/**
 * Twitter button Hashtag markup.
 */
function sharebuttons_twitter_mention($url) {

  // Get variables.
  $lang = variable_get('sharebuttons_twitter_language');
  $mention = check_plain(variable_get('sharebuttons_twitter_mention'));
  $text = check_plain(variable_get('sharebuttons_twitter_mention_text'));
  $name = check_plain(variable_get('sharebuttons_twitter_mention_recommend'));
  $size = variable_get('sharebuttons_twitter_size') ? ' data-size="large"' : '';
  $count = variable_get('sharebuttons_twitter_count') ? 'true' : 'false';
  $tailor = variable_get('sharebuttons_twitter_tailoring') ? 'true' : 'false';

  // Output button.
  $markup = '<a class="twitter-mention-button" href="https://twitter.com/intent/tweet?';
  $markup .= 'screen_name=' . urlencode($mention);
  $markup .= '&text=' . urlencode($text) . '"';
  $markup .= ' data-related="' . $name . '"';
  $markup .= ' data-lang="' . $lang . '"';
  $markup .= $size;
  $markup .= ' data-show-count="' . $count . '"';
  $markup .= ' data-dnt="' . $tailor . '"';
  $markup .= '>@Mention</a>';
  return $markup;
}


/**
 * Twitter settings form.
 */
function sharebuttons_twitter_form($form, &$form_state) {

  $form['twitter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter settings'),
  );

  $form['twitter']['sharebuttons_twitter_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to enable the Twitter button'),
    '#default_value' => variable_get('sharebuttons_twitter_enabled', 0),
  );

  $form['twitter']['sharebuttons_twitter_type'] = array(
    '#type' => 'radios',
    '#title' => t('Choose which type of button you want to enable'),
    '#default_value' => variable_get('sharebuttons_twitter_type', 'share'),
    '#required' => TRUE,
    '#options' => array(
      'share' => t('Share'),
      'follow' => t('@Follow'),
      'hashtag' => t('#Hashtag'),
      'mention' => t('@Mention'),
    ),
  );

  $form['twitter']['sharebuttons_twitter_size'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable large button'),
    '#default_value' => variable_get('sharebuttons_twitter_size', 0),
  );

  $form['twitter']['sharebuttons_twitter_count'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable to show counter'),
    '#options' => sharebuttons_twitter_settings('count'),
    '#default_value' => variable_get('sharebuttons_twitter_count', 0),
  );

  $form['twitter']['sharebuttons_twitter_tailoring'] = array(
    '#type' => 'checkbox',
    '#title' => t('Opt-out of tailoring Twitter <a href="https://support.twitter.com/articles/20169421">[?]</a>'),
    '#default_value' => variable_get('sharebuttons_twitter_tailoring', 0),
  );

  $form['twitter']['sharebuttons_twitter_language'] = array(
    '#type' => 'select',
    '#title' => t('Select the language for the button'),
    '#options' => sharebuttons_twitter_settings('language'),
    '#default_value' => variable_get('sharebuttons_twitter_language', 'en'),
    '#required' => TRUE,
  );

  // Share specific options.
  $form['twitter']['share'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter Share button settings'),
  );

  $form['twitter']['share']['sharebuttons_twitter_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Set this if you want a fixed url'),
    '#description' => t('Leave empty if you want to use the node/page-url'),
    '#attributes' => array('placeholder' => 'https://example.com'),
    '#default_value' => variable_get('sharebuttons_twitter_url'),
  );

  $form['twitter']['share']['sharebuttons_twitter_via'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the tweet @Via'),
    '#default_value' => variable_get('sharebuttons_twitter_via'),
    '#attributes' => array('placeholder' => 'twitterid'),
  );

  $form['twitter']['share']['sharebuttons_twitter_recommend'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the tweet @Recommend'),
    '#default_value' => variable_get('sharebuttons_twitter_recommend'),
    '#attributes' => array('placeholder' => 'twitterid'),
  );

  $form['twitter']['share']['sharebuttons_twitter_hashtags'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the tweet #Hashtag)'),
    '#default_value' => variable_get('sharebuttons_twitter_hashtags'),
    '#attributes' => array('placeholder' => 'hashtag'),
    '#description' => t('Multiple values allowed, comma separated'),
  );

  $form['twitter']['share']['sharebuttons_twitter_width'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#title' => t('Specify/override width for the button'),
    '#description' => t('Leave empty for autowidth (plugin decides) <a href="https://developers.twitter.com/+/web/+1button/#inline-annotation">[?]</a>'),
    '#attributes' => array('placeholder' => '200'),
  );

  // Follow specific options.
  $form['twitter']['follow'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter @Follow button settings'),
  );

  $form['twitter']['follow']['sharebuttons_twitter_follow_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter username you want to follow'),
    '#default_value' => variable_get('sharebuttons_twitter_follow_name'),
    '#attributes' => array('placeholder' => 'username'),
  );

  $form['twitter']['follow']['sharebuttons_twitter_follow_text'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to show username on button'),
    '#default_value' => variable_get('sharebuttons_twitter_follow_text', 0),
  );

  // Hashtag specific options.
  $form['twitter']['hashtag'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter #Hashtag button settings'),
  );

  $form['twitter']['hashtag']['sharebuttons_twitter_hashtag'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter #hashtag you want to display.'),
    '#default_value' => variable_get('sharebuttons_twitter_hashtag'),
    '#attributes' => array('placeholder' => 'TwitterStories'),
  );

  $form['twitter']['hashtag']['sharebuttons_twitter_hashtag_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Tweet default text'),
    '#default_value' => variable_get('sharebuttons_twitter_hashtag_text'),
    '#attributes' => array('placeholder' => t('My story is ...')),
    '#description' => t('This will be the default text of the tweet, e.g. "My story is ..."'),
  );

  $form['twitter']['hashtag']['sharebuttons_twitter_hashtag_recommend'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the tweet @Recommend'),
    '#default_value' => variable_get('sharebuttons_twitter_hashtag_recommend'),
    '#attributes' => array('placeholder' => 'twitterid'),
    '#description' => t('Multiple values allowed, comma separated'),
  );

  $form['twitter']['hashtag']['sharebuttons_twitter_hashtag_url'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to show url in Tweet text'),
    '#description' => t('By default only the Tweet default text will be shared'),
    '#default_value' => variable_get('sharebuttons_twitter_hashtag_url', 0),
  );

  // Hashtag specific options.
  $form['twitter']['mention'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter @Mention button settings'),
  );

  $form['twitter']['mention']['sharebuttons_twitter_mention'] = array(
    '#type' => 'textfield',
    '#title' => t('Twitter @Mention username you want to display.'),
    '#default_value' => variable_get('sharebuttons_twitter_mention'),
    '#attributes' => array('placeholder' => 'username'),
  );

  $form['twitter']['mention']['sharebuttons_twitter_mention_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Tweet default text'),
    '#default_value' => variable_get('sharebuttons_twitter_mention_text'),
    '#attributes' => array('placeholder' => t('My story is ...')),
    '#description' => t('This will be the default text of the tweet, e.g. "My story is ..."'),
  );

  $form['twitter']['mention']['sharebuttons_twitter_mention_recommend'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the tweet @Recommend'),
    '#default_value' => variable_get('sharebuttons_twitter_mention_recommend'),
    '#attributes' => array('placeholder' => 'twitterid'),
    '#description' => t('Multiple values allowed, comma separated'),
  );

  return system_settings_form($form);
}


/**
 * Implements hook_form_validate().
 */
function sharebuttons_twitter_form_validate($form, &$form_state) {

  // Validate url.
  if ($form_state['values']['sharebuttons_twitter_url'] && !valid_url($form_state['values']['sharebuttons_twitter_url'], TRUE)) {
    form_set_error('sharebuttons_twitter_url', t('Invalid url: start with http:// or https://.'));
  }

  // Validate width.
  if ($form_state['values']['sharebuttons_twitter_width'] && !is_numeric($form_state['values']['sharebuttons_twitter_width'])) {
    form_set_error('sharebuttons_twitter_width', t('Invalid width: enter a number, e.g. 200.'));
  }

  // Validate textinput.
  if ($form_state['values']['sharebuttons_twitter_via'] && !preg_match('/^[a-zA-Z0-9_]*$/', $form_state['values']['sharebuttons_twitter_via'])) {
    form_set_error('sharebuttons_twitter_via', t('Invalid @Via, only use letters, numbers and underscores'));
  }
  if ($form_state['values']['sharebuttons_twitter_recommend'] && !preg_match('/^[a-zA-Z0-9_]*$/', $form_state['values']['sharebuttons_twitter_recommend'])) {
    form_set_error('sharebuttons_twitter_recommend', t('Invalid @recommend, only use letters, numbers and underscores'));
  }
  if ($form_state['values']['sharebuttons_twitter_hashtags'] && !preg_match('/^[a-zA-Z0-9_,]*$/', $form_state['values']['sharebuttons_twitter_hashtags'])) {
    form_set_error('sharebuttons_twitter_hashtags', t('Invalid #Hashtag, only use letters, numbers, underscores and commas'));
  }
  if ($form_state['values']['sharebuttons_twitter_follow_name'] && !preg_match('/^[a-zA-Z0-9_]*$/', $form_state['values']['sharebuttons_twitter_follow_name'])) {
    form_set_error('sharebuttons_twitter_follow_name', t('Invalid Username, only use letters, numbers and underscores'));
  }
  if ($form_state['values']['sharebuttons_twitter_hashtag_recommend'] && !preg_match('/^[a-zA-Z0-9_,]*$/', $form_state['values']['sharebuttons_twitter_hashtag_recommend'])) {
    form_set_error('sharebuttons_twitter_hashtag_recommend', t('Invalid @recommend, only use letters, numbers, underscores and commas'));
  }
  if ($form_state['values']['sharebuttons_twitter_hashtag'] && !preg_match('/^[a-zA-Z0-9_,]*$/', $form_state['values']['sharebuttons_twitter_hashtag'])) {
    form_set_error('sharebuttons_twitter_hashtag', t('Invalid #Hashtag, only use letters, numbers and underscores'));
  }
  if ($form_state['values']['sharebuttons_twitter_mention'] && !preg_match('/^[a-zA-Z0-9_]*$/', $form_state['values']['sharebuttons_twitter_mention'])) {
    form_set_error('sharebuttons_twitter_mention', t('Invalid @Mention, only use letters, numbers and underscores'));
  }

  // Validate follow button.
  if ($form_state['values']['sharebuttons_twitter_type'] == 'follow' && !$form_state['values']['sharebuttons_twitter_follow_name']) {
    form_set_error('sharebuttons_twitter_follow_name', t('Set a username'));
  }

  // Validate hashtag button.
  if ($form_state['values']['sharebuttons_twitter_type'] == 'hashtag' && !$form_state['values']['sharebuttons_twitter_hashtag']) {
    form_set_error('sharebuttons_twitter_hashtag', t('Set a hashtag'));
  }

  // Validate mention button.
  if ($form_state['values']['sharebuttons_twitter_type'] == 'mention' && !$form_state['values']['sharebuttons_twitter_mention']) {
    form_set_error('sharebuttons_twitter_mention', t('Set a @Mention username'));
  }
}


/**
 * Twitter options.
 */
function sharebuttons_twitter_settings($option) {

  if ($option == 'language') {
    $items = array(
      'ar' => t('Arabic'),
      'eu' => t('Basque'),
      'ca' => t('Catalan'),
      'cs' => t('Czech'),
      'da' => t('Danish'),
      'nl' => t('Dutch'),
      'en' => t('English US'),
      'en-gb' => t('English UK'),
      'fa' => t('Farsi'),
      'fil' => t('Filipino'),
      'fi' => t('Finnish'),
      'fr' => t('French'),
      'gl' => t('Galician'),
      'de' => t('German'),
      'el' => t('Greek'),
      'he' => t('Hebrew'),
      'hi' => t('Hindi'),
      'hu' => t('Hungarian'),
      'id' => t('Indonesian'),
      'it' => t('Italian'),
      'ja' => t('Japanese'),
      'ko' => t('Korean'),
      'lv' => t('Latvian'),
      'lolc' => t('Lolcatz'),
      'msa' => t('Malay'),
      'no' => t('Norwegian'),
      'nb' => t('Norwegian Bokmål'),
      'pl' => t('Polish'),
      'pt' => t('Portuguese'),
      'ro' => t('Romainian'),
      'ru' => t('Russian'),
      'zh-cn' => t('Simplified Chinese'),
      'sl' => t('Slovene'),
      'es' => t('Spanish'),
      'sv' => t('Swedish'),
      'th' => t('Thai'),
      'zh-tw' => t('Traditional Chinese'),
      'tr' => t('Turkish'),
      'uk' => t('Ukranian'),
      'ur' => t('Urdu'),
    );
    asort($items);
    return $items;
  }

}
