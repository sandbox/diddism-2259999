<?php
/**
 * @file
 * Implements Tumblr plugin.
 */

/**
 * Tumblr button render.
 */
function sharebuttons_tumblr($url) {

  // Get variables.
  $name = check_plain(variable_get('sharebuttons_tumblr_name'));
  $description = check_plain(variable_get('sharebuttons_tumblr_description'));
  $layout = variable_get('sharebuttons_tumblr_layout');

  // Get width for buttons.
  switch ($layout) {

    case 'share_1':
    case 'share_1T':
      $width = '81';
      break;

    case 'share_2':
    case 'share_2T':
      $width = '61';
      break;

    case 'share_3':
    case 'share_3T':
      $width = '129';
      break;

    case 'share_4':
    case 'share_4T':
      $width = '20';
      break;

  }

  // Add javascript.
  drupal_add_js('http://platform.tumblr.com/v1/share.js');

  // Output button.
  $markup = '<a href="http://www.tumblr.com/share/link?';
  $markup .= 'url=' . urlencode($url);
  $markup .= '&name=' . urlencode($name);
  $markup .= '&description=' . urlencode($description) . '"';
  $markup .= ' style="display:inline-block; text-indent:-9999px; overflow:hidden;';
  $markup .= ' width:' . $width . 'px;';
  $markup .= ' height:20px;';
  $markup .= ' background:url(http://platform.tumblr.com/v1/' . $layout . '.png) top left no-repeat transparent;"';
  $markup .= ' >Share on tumblr</a>';
  return $markup;

}


/**
 * Tumblr settings form.
 */
function sharebuttons_tumblr_form($form, &$form_state) {

  $form['tumblr'] = array(
    '#type' => 'fieldset',
    '#title' => t('Tumblr settings'),
  );

  $form['tumblr']['sharebuttons_tumblr_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to enable the Tumblr button'),
    '#default_value' => variable_get('sharebuttons_tumblr_enabled', 0),
  );

  $form['tumblr']['sharebuttons_tumblr_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Set this if you want a fixed url'),
    '#description' => t('Leave empty if you want to use the node/page-url'),
    '#attributes' => array('placeholder' => 'https://example.com'),
    '#default_value' => variable_get('sharebuttons_tumblr_url'),
  );

  $form['tumblr']['sharebuttons_tumblr_layout'] = array(
    '#type' => 'radios',
    '#title' => t('Select the layout for the button'),
    '#options' => sharebuttons_tumblr_settings('layout'),
    '#default_value' => variable_get('sharebuttons_tumblr_layout', 'share_1'),
    '#required' => TRUE,
  );

  // @todo Implement node-title here
  //   need to add $node to function-calls.
  $form['tumblr']['sharebuttons_tumblr_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the name for the sharing text'),
    '#description' => t('Leave empty to share only the link'),
    '#attributes' => array('placeholder' => t('My awesome name')),
  );

  // @todo Implement shortened node-summary here
  //   need to add $node to function-calls.
  $form['tumblr']['sharebuttons_tumblr_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Set a description for the link'),
    '#description' => t('Leave empty to share only the link'),
    '#attributes' => array('placeholder' => t('My awesome text')),
  );

  return system_settings_form($form);
}


/**
 * Implements hook_form_validate().
 */
function sharebuttons_tumblr_form_validate($form, &$form_state) {

  // Validate url.
  if ($form_state['values']['sharebuttons_tumblr_url'] && !valid_url($form_state['values']['sharebuttons_tumblr_url'], TRUE)) {
    form_set_error('sharebuttons_tumblr_url', t('Invalid url: start with http:// or https://.'));
  }

}


/**
 * Tumblr options.
 */
function sharebuttons_tumblr_settings($option) {

  switch ($option) {

    case 'layout':
      $items = array(
        'share_1' => '<img src="http://platform.tumblr.com/v1/share_1.png" title="width=81px;">',
        'share_2' => '<img src="http://platform.tumblr.com/v1/share_2.png" title="width=61px;">',
        'share_3' => '<img src="http://platform.tumblr.com/v1/share_3.png" title="width=129px;">',
        'share_4' => '<img src="http://platform.tumblr.com/v1/share_4.png" title="width=20px;">',
        'share_1T' => '<img src="http://platform.tumblr.com/v1/share_1T.png" title="width=81px;">',
        'share_2T' => '<img src="http://platform.tumblr.com/v1/share_2T.png" title="width=61px;">',
        'share_3T' => '<img src="http://platform.tumblr.com/v1/share_3T.png" title="width=129px;">',
        'share_4T' => '<img src="http://platform.tumblr.com/v1/share_4T.png" title="width=20px;">',
      );
      return $items;

  }
}
