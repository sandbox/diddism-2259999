<?php
/**
 * @file
 * Implements Google +1 and Google share plugin.
 */

/**
 * Google's +1 button render.
 */
function sharebuttons_google($url) {

  // Load javascript.
  $action = variable_get('sharebuttons_google_plusone_enabled') ? 'plusone' : 'platform';
  $settings['lang'] = variable_get('sharebuttons_google_language');
  $settings['action'] = $action;
  drupal_add_js(array('sharebuttons_google' => $settings), 'setting');
  drupal_add_js(drupal_get_path('module', 'sharebuttons') . '/js/google.js');

  // Get the correct markup.
  $button = 'sharebuttons_google_' . variable_get('sharebuttons_google_type');
  return $button($url);
}


/**
 * Google +1 button markup.
 */
function sharebuttons_google_plusone($url) {

  // Get variables.
  $size = variable_get('sharebuttons_google_plusone_size');
  if ($size == 'standard') {
    $size = '';
  }
  else {
    $size = ' data-size="' . $size . '"';
  }
  $width = variable_get('sharebuttons_google_width') ? ' data-width="' . variable_get('sharebuttons_google_width') . '"' : '';
  $layout = variable_get('sharebuttons_google_plusone_layout');

  // Output google +1 button.
  $markup = '<div class="g-plusone"';
  $markup .= ' data-href="' . $url . '"';
  $markup .= $size;
  $markup .= ' data-annotation="' . $layout . '"';
  $markup .= $width;
  $markup .= '></div>';
  return $markup;
}


/**
 * Google share button markup.
 */
function sharebuttons_google_share($url) {

  // Get variables.
  $size = variable_get('sharebuttons_google_share_size');
  if ($size == 'standard') {
    $size = '';
  }
  $width = variable_get('sharebuttons_google_width') ? ' data-width="' . variable_get('sharebuttons_google_width') . '"' : '';
  $layout = variable_get('sharebuttons_google_share_layout');

  switch ($layout) {

    case 'inline':
      $layout = '';
      break;

    case 'vertical-bubble':
      $size = '60';
      break;

  }

  // Output google +1 button.
  $markup = '<div class="g-plus" data-action="share"';
  $markup .= ' data-href="' . $url . '"';
  $markup .= ' data-size="' . $size . '"';
  $markup .= ' data-annotation="' . $layout . '"';
  $markup .= $width;
  $markup .= '></div>';
  return $markup;
}


/**
 * Google's button form.
 */
function sharebuttons_google_form($form, &$form_state) {

  $form['google'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google +1 settings'),
  );

  $form['google']['sharebuttons_google_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to enable the Google provider'),
    '#default_value' => variable_get('sharebuttons_google_enabled', 0),
  );

  $form['google']['sharebuttons_google_type'] = array(
    '#type' => 'radios',
    '#title' => t('Choose which type of button you want to enable'),
    '#default_value' => variable_get('sharebuttons_google_type', 'plusone'),
    '#required' => TRUE,
    '#options' => array(
      'plusone' => '+1',
      'share' => t('Share'),
    ),
  );

  $form['google']['sharebuttons_google_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Set this if you want a fixed url'),
    '#description' => t('Leave empty if you want to use the node/page-url'),
    '#attributes' => array('placeholder' => 'https://example.com'),
    '#default_value' => variable_get('sharebuttons_google_url'),
  );

  $form['google']['sharebuttons_google_language'] = array(
    '#type' => 'select',
    '#title' => t('Select the language for the button'),
    '#options' => sharebuttons_google_settings('both', 'language'),
    '#default_value' => variable_get('sharebuttons_google_language', 'en-US'),
    '#required' => TRUE,
  );

  $form['google']['sharebuttons_google_width'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#title' => t('Specify/override width for the button'),
    '#description' => t('Leave empty for autowidth (plugin decides) <a href="https://developers.google.com/+/web/+1button/#inline-annotation">[?]</a>'),
    '#attributes' => array('placeholder' => '200'),
  );

  // Settings for Google +1.
  $form['google']['plusone'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google +1 button'),
  );

  $form['google']['plusone']['sharebuttons_google_plusone_size'] = array(
    '#type' => 'select',
    '#title' => t('Select the size for the button'),
    '#options' => sharebuttons_google_settings('plusone', 'size'),
    '#default_value' => variable_get('sharebuttons_google_plusone_size', 'medium'),
    '#required' => TRUE,
  );

  $form['google']['plusone']['sharebuttons_google_plusone_layout'] = array(
    '#type' => 'select',
    '#title' => t('Select the layout for the button'),
    '#options' => sharebuttons_google_settings('plusone', 'layout'),
    '#default_value' => variable_get('sharebuttons_google_plusone_layout', 'none'),
    '#required' => TRUE,
  );

  // Settings for Google share.
  $form['google']['share'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Share button'),
  );

  $form['google']['share']['sharebuttons_google_share_size'] = array(
    '#type' => 'select',
    '#title' => t('Select the size for the button'),
    '#options' => sharebuttons_google_settings('share', 'size'),
    '#default_value' => variable_get('sharebuttons_google_share_size', 'standard'),
    '#required' => TRUE,
  );

  $form['google']['share']['sharebuttons_google_share_layout'] = array(
    '#type' => 'select',
    '#title' => t('Select the layout for the button'),
    '#options' => sharebuttons_google_settings('share', 'layout'),
    '#default_value' => variable_get('sharebuttons_google_share_layout', 'none'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}


/**
 * Implements hook_form_validate().
 */
function sharebuttons_google_form_validate($form, &$form_state) {

  // Validate url.
  if ($form_state['values']['sharebuttons_google_url'] && !valid_url($form_state['values']['sharebuttons_google_url'], TRUE)) {
    form_set_error('sharebuttons_google_url', t('Invalid url: start with http:// or https://.'));
  }

  // Validate width.
  if ($form_state['values']['sharebuttons_google_width'] && !is_numeric($form_state['values']['sharebuttons_google_width'])) {
    form_set_error('sharebuttons_google_width', t('Invalid width: enter a number, e.g. 200.'));
  }

}


/**
 * Google's button options.
 */
function sharebuttons_google_settings($type, $option) {

  // Google +1 options.
  if ($type == 'plusone') {
    switch ($option) {

      case 'size':
        $items = array(
          'small' => t('Small'),
          'medium' => t('Medium'),
          'standard' => t('Standard'),
          'tall' => t('Tall'),
        );
        return $items;

      case 'layout':
        $items = array(
          'none' => t('No counter'),
          'inline' => t('Inline counter'),
          'balloon' => t('Balloon'),
        );
        return $items;

    }
  }

  if ($type == 'share') {
    switch ($option) {

      case 'size':
        $items = array(
          '15' => t('Small'),
          // Standard is data-height=''.
          'standard' => t('Standard'),
          '24' => t('Tall'),
        );
        return $items;

      case 'layout':
        $items = array(
          'none' => t('No counter'),
          'bubble' => t('Inline counter'),
          'vertical-bubble' => t('Balloon'),
          // Inline is data-annotation=''.
          'inline' => t('Inline text'),
        );
        return $items;

    }
  }

  // Return language.
  if ($option == 'language') {
    $items = array(
      'af' => t('Afrikaans'),
      'am' => t('Amharic'),
      'ar' => t('Arabic'),
      'eu' => t('Basque'),
      'bn' => t('Bengali'),
      'bg' => t('Bulgarian'),
      'ca' => t('Catalan'),
      'zh-HK' => t('Chinese (Hong Kong)'),
      'zh-CN' => t('Chinese (Simplified)'),
      'zh-TW' => t('Chinese (Traditional)'),
      'hr' => t('Croatian'),
      'cs' => t('Czech'),
      'da' => t('Danish'),
      'nl' => t('Dutch'),
      'en-GB' => t('English (UK)'),
      'en-US' => t('English (US)'),
      'et' => t('Estonian'),
      'fil' => t('Filipino'),
      'fi' => t('Finnish'),
      'fr' => t('French'),
      'fr-CA' => t('French (Canadian)'),
      'gl' => t('Galician'),
      'de' => t('German'),
      'el' => t('Greek'),
      'gu' => t('Gujarati'),
      'iw' => t('Hebrew'),
      'hi' => t('Hindi'),
      'hu' => t('Hungarian'),
      'is' => t('Icelandic'),
      'id' => t('Indonesian'),
      'it' => t('Italian'),
      'ja' => t('Japanese'),
      'kn' => t('Kannada'),
      'ko' => t('Korean'),
      'lv' => t('Latvian'),
      'lt' => t('Lithuanian'),
      'ms' => t('Malay'),
      'ml' => t('Malayalam'),
      'mr' => t('Marathi'),
      'no' => t('Norwegian'),
      'fa' => t('Persian'),
      'pl' => t('Polish'),
      'pt-BR' => t('Portuguese (Brazil)'),
      'pt-PT' => t('Portuguese (Portugal)'),
      'ro' => t('Romanian'),
      'ru' => t('Russian'),
      'sr' => t('Serbian'),
      'sk' => t('Slovak'),
      'sl' => t('Slovenian'),
      'es' => t('Spanish'),
      'es-419' => t('Spanish (Latin America)'),
      'sw' => t('Swahili'),
      'sv' => t('Swedish'),
      'ta' => t('Tamil'),
      'te' => t('Telugu'),
      'th' => t('Thai'),
      'tr' => t('Turkish'),
      'uk' => t('Ukrainian'),
      'ur' => t('Urdu'),
      'vi' => t('Vietnamese'),
      'zu' => t('Zulu'),
    );
    asort($items);
    return $items;
  }

}
