<?php
/**
 * @file
 * Implements Facebook plugin.
 */

/**
 * Facebook like button render.
 */
function sharebuttons_facebook($url) {

  // Load javascript.
  $settings['lang'] = variable_get('sharebuttons_facebook_language');
  drupal_add_js(array('sharebuttons_facebook' => $settings), 'setting');
  drupal_add_js(drupal_get_path('module', 'sharebuttons') . '/js/facebook.js');

  $button = 'sharebuttons_facebook_' . variable_get('sharebuttons_facebook_type');
  return $button($url);
}


/**
 * Facebook Like/Recommend button markup.
 */
function sharebuttons_facebook_like($url) {

  // Get variables.
  $width = variable_get('sharebuttons_facebook_width');
  $color = variable_get('sharebuttons_facebook_color');
  $layout = variable_get('sharebuttons_facebook_like_layout');
  $action = variable_get('sharebuttons_facebook_like_action');
  $faces = variable_get('sharebuttons_facebook_like_faces');
  $share = variable_get('sharebuttons_facebook_like_share');

  // Output button.
  $markup = '<div id="fb-root"></div><div class="fb-like"';
  $markup .= ' data-href="' . $url . '"';
  $markup .= ' data-layout="' . $layout . '"';
  $markup .= ' data-action="' . $action . '"';
  $markup .= ' data-share="' . $share . '"';
  $markup .= ' data-show-faces="' . $faces . '"';
  $markup .= ' data-width="' . $width . '"';
  $markup .= ' data-colorscheme="' . $color . '"';
  $markup .= '></div>';
  return $markup;
}


/**
 * Facebook Share button markup.
 */
function sharebuttons_facebook_share($url) {

  // Get variables.
  $width = variable_get('sharebuttons_facebook_width');
  $color = variable_get('sharebuttons_facebook_color');
  $layout = variable_get('sharebuttons_facebook_share_layout');

  // Output button.
  $markup = '<div id="fb-root"><div class="fb-share-button"';
  $markup .= ' data-href="' . $url . '"';
  $markup .= ' data-type="' . $layout . '"';
  $markup .= ' data-width="' . $width . '"';
  $markup .= ' data-colorscheme="' . $color . '"';
  $markup .= '></div>';
  return $markup;
}


/**
 * Facebook settings form.
 */
function sharebuttons_facebook_form($form, &$form_state) {

  $form['facebook'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook settings'),
  );

  $form['facebook']['sharebuttons_facebook_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to enable the Facebook provider'),
    '#default_value' => variable_get('sharebuttons_facebook_enabled', 0),
  );

  $form['facebook']['sharebuttons_facebook_type'] = array(
    '#type' => 'radios',
    '#title' => t('Choose which type of button you want to enable'),
    '#default_value' => variable_get('sharebuttons_facebook_type', 'like'),
    '#required' => TRUE,
    '#options' => array(
      'like' => t('Like'),
      'share' => t('Share'),
    ),
  );

  $form['facebook']['sharebuttons_facebook_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Set this if you want a fixed url'),
    '#description' => t('Leave empty if you want to use the node/page-url. You can set your Facebookpage-url here to let people like that page'),
    '#attributes' => array('placeholder' => 'https://example.com'),
    '#default_value' => variable_get('sharebuttons_facebook_url'),
  );

  $form['facebook']['sharebuttons_facebook_language'] = array(
    '#type' => 'select',
    '#title' => t('Select the language for the button'),
    '#options' => sharebuttons_facebook_settings('both', 'language'),
    '#default_value' => variable_get('sharebuttons_facebook_language', 'en_US'),
    '#required' => TRUE,
  );

  $form['facebook']['sharebuttons_facebook_color'] = array(
    '#type' => 'select',
    '#title' => t('Select the colorscheme of the plugin'),
    '#options' => sharebuttons_facebook_settings('both', 'color'),
    '#default_value' => variable_get('sharebuttons_facebook_color', 'light'),
    '#required' => TRUE,
  );

  $form['facebook']['sharebuttons_facebook_width'] = array(
    '#type' => 'textfield',
    '#size' => 3,
    '#maxlength' => 3,
    '#title' => t('Specify/override width for the button'),
    '#default_value' => variable_get('sharebuttons_facebook_width'),
    '#description' => t('Leave empty for autowidth (plugin decides) <a href="https://developers.facebook.com/docs/plugins/like-button/#faqlayout">[?]</a>'),
    '#attributes' => array('placeholder' => '200'),
  );

  $form['facebook']['like'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook Like/Recommend button'),
  );

  $form['facebook']['like']['sharebuttons_facebook_like_layout'] = array(
    '#type' => 'select',
    '#title' => t('Select the layout for the button'),
    '#options' => sharebuttons_facebook_settings('like', 'layout'),
    '#default_value' => variable_get('sharebuttons_facebook_like_layout', 'button'),
    '#required' => TRUE,
  );

  $form['facebook']['like']['sharebuttons_facebook_like_action'] = array(
    '#type' => 'select',
    '#title' => t('Select the action for the button'),
    '#options' => sharebuttons_facebook_settings('like', 'action'),
    '#default_value' => variable_get('sharebuttons_facebook_like_action', 'like'),
    '#required' => TRUE,
  );

  $form['facebook']['like']['sharebuttons_facebook_like_faces'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to display Friends faces'),
    '#default_value' => variable_get('sharebuttons_facebook_like_faces', 0),
  );

  $form['facebook']['like']['sharebuttons_facebook_like_share'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to include Facebook Share'),
    '#default_value' => variable_get('sharebuttons_facebook_like_share', 0),
  );

  $form['facebook']['share'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook Share button'),
  );

  $form['facebook']['share']['sharebuttons_facebook_share_layout'] = array(
    '#type' => 'select',
    '#title' => t('Select the layout for the button'),
    '#options' => sharebuttons_facebook_settings('share', 'layout'),
    '#default_value' => variable_get('sharebuttons_facebook_share_layout', 'button'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}


/**
 * Implements hook_form_validate().
 */
function sharebuttons_facebook_form_validate($form, &$form_state) {

  // Validate url.
  if ($form_state['values']['sharebuttons_facebook_url'] && !valid_url($form_state['values']['sharebuttons_facebook_url'], TRUE)) {
    form_set_error('sharebuttons_facebook_url', t('Invalid url: start with http:// or https://.'));
  }

  // Validate width.
  if ($form_state['values']['sharebuttons_facebook_width'] && !is_numeric($form_state['values']['sharebuttons_facebook_width'])) {
    form_set_error('sharebuttons_facebook_width', t('Invalid width: enter a number, e.g. 200.'));
  }

}


/**
 * Facebook button options.
 */
function sharebuttons_facebook_settings($type, $option) {

  // Facebook Like button options.
  if ($type == 'like') {
    switch ($option) {

      case 'action':
        $items = array(
          'like' => t('Like'),
          'recommend' => t('Recommend'),
        );
        return $items;

      case 'layout':
        $items = array(
          'standard' => t('Standard'),
          'box_count' => t('Box count'),
          'button_count' => t('Button count'),
          'button' => t('Button'),
        );
        return $items;

    }
  }

  // Facebook Share button options.
  if ($type == 'share') {
    switch ($option) {

      case 'layout':
        $items = array(
          'box_count' => t('Box count'),
          'button_count' => t('Button count'),
          'button' => t('Button'),
          'icon' => t('Icon'),
          'icon-link' => t('Icon with link'),
          'link' => t('Link only'),
        );
        return $items;

    }
  }

  if ($option == 'color') {
    $items = array(
      'light' => t('Light'),
      'dark' => t('Dark'),
    );
    return $items;
  }

  if ($option == 'language') {
    $items = array(
      'af_ZA' => t('Afrikaans'),
      'gn_PY' => t('Guaraní'),
      'ay_BO' => t('Aymara'),
      'az_AZ' => t('Azeri'),
      'id_ID' => t('Indonesian'),
      'ms_MY' => t('Malay'),
      'jv_ID' => t('Javanese'),
      'bs_BA' => t('Bosnian'),
      'ca_ES' => t('Catalan'),
      'cs_CZ' => t('Czech'),
      'ck_US' => t('Cherokee'),
      'cy_GB' => t('Welsh'),
      'da_DK' => t('Danish'),
      'se_NO' => t('Northern Sámi'),
      'de_DE' => t('German'),
      'et_EE' => t('Estonian'),
      'en_IN' => t('English (India)'),
      'en_PI' => t('English (Pirate)'),
      'en_GB' => t('English (UK)'),
      'en_UD' => t('English (Upside Down)'),
      'en_US' => t('English (US)'),
      'es_LA' => t('Spanish'),
      'es_CL' => t('Spanish (Chile)'),
      'es_CO' => t('Spanish (Colombia)'),
      'es_ES' => t('Spanish (Spain)'),
      'es_MX' => t('Spanish (Mexico)'),
      'es_VE' => t('Spanish (Venezuela)'),
      'eo_EO' => t('Esperanto'),
      'eu_ES' => t('Basque'),
      'tl_PH' => t('Filipino'),
      'fo_FO' => t('Faroese'),
      'fr_FR' => t('French (France)'),
      'fr_CA' => t('French (Canada)'),
      'fy_NL' => t('Frisian'),
      'ga_IE' => t('Irish'),
      'gl_ES' => t('Galician'),
      'ko_KR' => t('Korean'),
      'hr_HR' => t('Croatian'),
      'xh_ZA' => t('Xhosa'),
      'zu_ZA' => t('Zulu'),
      'is_IS' => t('Icelandic'),
      'it_IT' => t('Italian'),
      'ka_GE' => t('Georgian'),
      'sw_KE' => t('Swahili'),
      'tl_ST' => t('Klingon'),
      'ku_TR' => t('Kurdish'),
      'lv_LV' => t('Latvian'),
      'fb_LT' => t('Leet Speak'),
      'lt_LT' => t('Lithuanian'),
      'li_NL' => t('Limburgish'),
      'la_VA' => t('Latin'),
      'hu_HU' => t('Hungarian'),
      'mg_MG' => t('Malagasy'),
      'mt_MT' => t('Maltese'),
      'nl_NL' => t('Dutch'),
      'nl_BE' => t('Dutch (België)'),
      'ja_JP' => t('Japanese'),
      'nb_NO' => t('Norwegian (bokmal)'),
      'nn_NO' => t('Norwegian (nynorsk)'),
      'uz_UZ' => t('Uzbek'),
      'pl_PL' => t('Polish'),
      'pt_BR' => t('Portuguese (Brazil)'),
      'pt_PT' => t('Portuguese (Portugal)'),
      'qu_PE' => t('Quechua'),
      'ro_RO' => t('Romanian'),
      'rm_CH' => t('Romansh'),
      'ru_RU' => t('Russian'),
      'sq_AL' => t('Albanian'),
      'sk_SK' => t('Slovak'),
      'sl_SI' => t('Slovenian'),
      'so_SO' => t('Somali'),
      'fi_FI' => t('Finnish'),
      'sv_SE' => t('Swedish'),
      'th_TH' => t('Thai'),
      'vi_VN' => t('Vietnamese'),
      'tr_TR' => t('Turkish'),
      'zh_CN' => t('Simplified Chinese (China)'),
      'zh_TW' => t('Traditional Chinese (Taiwan)'),
      'zh_HK' => t('Traditional Chinese (Hong Kong)'),
      'el_GR' => t('Greek'),
      'gx_GR' => t('Classical Greek'),
      'be_BY' => t('Belarusian'),
      'bg_BG' => t('Bulgarian'),
      'kk_KZ' => t('Kazakh'),
      'mk_MK' => t('Macedonian'),
      'mn_MN' => t('Mongolian'),
      'sr_RS' => t('Serbian'),
      'tt_RU' => t('Tatar'),
      'tg_TJ' => t('Tajik'),
      'uk_UA' => t('Ukrainian'),
      'hy_AM' => t('Armenian'),
      'yi_DE' => t('Yiddish'),
      'he_IL' => t('Hebrew'),
      'ur_PK' => t('Urdu'),
      'ar_AR' => t('Arabic'),
      'ps_AF' => t('Pashto'),
      'fa_IR' => t('Persian'),
      'sy_SY' => t('Syriac'),
      'ne_NP' => t('Nepali'),
      'mr_IN' => t('Marathi'),
      'sa_IN' => t('Sanskrit'),
      'hi_IN' => t('Hindi'),
      'bn_IN' => t('Bengali'),
      'pa_IN' => t('Punjabi'),
      'gu_IN' => t('Gujarati'),
      'ta_IN' => t('Tamil'),
      'te_IN' => t('Telugu'),
      'kn_IN' => t('Kannada'),
      'ml_IN' => t('Malayalam'),
      'km_KH' => t('Khmer'),
    );
    asort($items);
    return $items;
  }
}
