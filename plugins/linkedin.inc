<?php
/**
 * @file
 * Implements LinkedIN plugin.
 */

/**
 * LinkedIN button render.
 */
function sharebuttons_linkedin($url) {

  // Get variables.
  $lang = variable_get('sharebuttons_linkedin_language');
  $layout = variable_get('sharebuttons_linkedin_layout');

  // Output button.
  $markup = '<script src="//platform.linkedin.com/in.js" type="text/javascript">';
  $markup .= 'lang: ' . $lang;
  $markup .= '</script><script type="IN/Share"';
  $markup .= ' data-url="' . $url . '"';
  $markup .= ' data-showZero="true"';

  if ($layout != 'none') {
    $markup .= ' data-counter="' . $layout . '"';
  }

  $markup .= '></script>';
  return $markup;

}


/**
 * LinkedIN settings form.
 */
function sharebuttons_linkedin_form($form, &$form_state) {

  $form['linkedin'] = array(
    '#type' => 'fieldset',
    '#title' => t('LinkedIN settings'),
  );

  $form['linkedin']['sharebuttons_linkedin_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check to enable the LinkedIN button'),
    '#default_value' => variable_get('sharebuttons_linkedin_enabled', 0),
  );

  $form['linkedin']['sharebuttons_linkedin_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Set this if you want a fixed url'),
    '#description' => t('Leave empty if you want to use the node/page-url'),
    '#attributes' => array('placeholder' => 'https://example.com'),
    '#default_value' => variable_get('sharebuttons_linkedin_url'),
  );

  $form['linkedin']['sharebuttons_linkedin_layout'] = array(
    '#type' => 'select',
    '#title' => t('Select the layout for the button'),
    '#options' => sharebuttons_linkedin_settings('layout'),
    '#default_value' => variable_get('sharebuttons_linkedin_layout', 'none'),
    '#required' => TRUE,
  );

  $form['linkedin']['sharebuttons_linkedin_language'] = array(
    '#type' => 'select',
    '#title' => t('Select the language for the button'),
    '#options' => sharebuttons_linkedin_settings('language'),
    '#default_value' => variable_get('sharebuttons_linkedin_language', 'en_US'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}


/**
 * Implements hook_form_validate().
 */
function sharebuttons_linkedin_form_validate($form, &$form_state) {

  // Validate url.
  if ($form_state['values']['sharebuttons_linkedin_url'] && !valid_url($form_state['values']['sharebuttons_linkedin_url'], TRUE)) {
    form_set_error('sharebuttons_linkedin_url', t('Invalid url: start with http:// or https://.'));
  }

}


/**
 * LinkedIN options.
 */
function sharebuttons_linkedin_settings($option) {

  switch ($option) {

    case 'layout':
      $items = array(
        'top' => t('Vertical (counter above button)'),
        'right' => t('Horizontal (counter right of button)'),
        'none' => t('No counter'),
      );
      return $items;

    case 'language':
      $items = array(
        'en_US' => t('English'),
        'fr_FR' => t('French'),
        'es_ES' => t('Spanish'),
        'ru_RU' => t('Russian'),
        'de_DE' => t('German'),
        'it_IT' => t('Italian'),
        'pt_BR' => t('Portuguese'),
        'ro_RO' => t('Romanian'),
        'tr_TR' => t('Turkish'),
        'ja_JP' => t('Japanese'),
        'in_ID' => t('Indonesian'),
        'ms_MY' => t('Malay'),
        'ko_KR' => t('Korean'),
        'sv_SE' => t('Swedish'),
        'cs_CZ' => t('Czech'),
        'nl_NL' => t('Dutch'),
        'pl_PL' => t('Polish'),
        'no_NO' => t('Norwegian'),
        'da_DK' => t('Danish'),
      );
      asort($items);
      return $items;

  }
}
