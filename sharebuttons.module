<?php
/**
 * @file
 * Implements sharebuttons module and imports plugins from pluginfolder.
 */

/**
 * Implements hook_help().
 */
function sharebuttons_help($path, $arg) {
  switch ($path) {
    case 'admin/help#sharebuttons':

      $markup = '<p>' . t("Sharebuttons is a module that provides interactive social media buttons. It uses the native HTML5 widget setup as recommended by those social media's, while providing as much admin-configurable options as they have. Needed javascript will be loaded asynchronously (where possible) in the HEAD-section of your page if nodes are loaded on which the buttons are enabled.") . '</p>' . t('Available providers are:') . '<ul>';

      // Get a list of plugins to show on help-page.
      $plugins = sharebuttons_plugins();
      foreach ($plugins as $name => $info) {
        $markup .= '<li>' . ucfirst($name) . '</li>';
      }

      $markup .= '</ul><p>' . t('Enable at least 1 provider and nodetype. Then you should see the provider-button appear on (full) nodes of that type.') . '</p>';
      return $markup;

  }
}

/**
 * Implements hook_menu().
 *
 * Build the settings-pages.
 */
function sharebuttons_menu() {

  $items['admin/config/services/sharebuttons'] = array(
    'title' => 'Sharebuttons',
    'type' => MENU_NORMAL_ITEM,
    'description' => 'Configuration for Sharebuttons module',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sharebuttons_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'sharebuttons.admin.inc',
  );

  $items['admin/config/services/sharebuttons/settings'] = array(
    'title' => 'Global settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/services/sharebuttons/settings/global'] = array(
    'title' => 'Global settings',
    'type' => MENU_LOCAL_TASK,
    'description' => 'Global settings',
    'access arguments' => array('administer site configuration'),
    'weight' => -1,
  );

  $plugins = sharebuttons_plugins();
  foreach ($plugins as $name => $info) {
    $items['admin/config/services/sharebuttons/settings/' . $name] = array(
      'title' => ucfirst($name),
      'type' => MENU_LOCAL_TASK,
      'page callback' => 'drupal_get_form',
      'access arguments' => array('administer site configuration'),
      'page arguments' => array('sharebuttons_' . $name . '_form'),
      'file' => 'plugins/' . $name . '.inc',
    );
  }

  return $items;
}


/**
 * Implements hook_theme().
 *
 * Use template for widget.
 */
function sharebuttons_theme() {
  return array(
    'sharebuttons-widget' => array(
      'template' => 'sharebuttons-widget',
      'path' => drupal_get_path('module', 'sharebuttons') . '/templates',
      'variables' => array('sharebuttons' => NULL, 'alignment' => NULL),
    ),
  );
}


/**
 * Add variables to theme.
 */
function sharebuttons_widget($url) {

  $plugins = sharebuttons_plugins();
  foreach ($plugins as $name => $info) {
    if (variable_get('sharebuttons_' . $name . '_enabled')) {

      // Import plugins and functions.
      $file = drupal_get_path('module', 'sharebuttons') . '/plugins/' . $name . '.inc';
      require_once $file;

      // Check for fixed url and sanitize.
      if (variable_get('sharebuttons_' . $name . '_url')) {
        $url = variable_get('sharebuttons_' . $name . '_url');
        $url = filter_xss_admin($url);
      }

      // Execute the plugins.
      $plugin = 'sharebuttons_' . $name;
      $sharebuttons[$name] = $plugin($url);

    }

  }

  drupal_add_css(drupal_get_path('module', 'sharebuttons') . '/css/sharebuttons.css');
  return $sharebuttons;

}


/**
 * Implements hook_node_view().
 *
 * Add widget to nodes.
 */
function sharebuttons_node_view($node, $view_mode = NULL) {

  // Return if no plugin is enabled.
  if (!sharebuttons_plugins_enabled()) {
    return;
  }

  // Check if widget needs to be displayed on teasers.
  $teaser_enabled = variable_get('sharebuttons_teaser');
  if ($view_mode == 'teaser' && !$teaser_enabled) {
    return;
  }

  // Check if $node->type is enabled and render widget if so.
  $types = variable_get('sharebuttons_nodes');
  if (!$types[$node->type]) {
    return;
  }
  else {
    $url = url('node/' . $node->nid, array('absolute' => TRUE));
    $sharebuttons = sharebuttons_widget($url);
    $alignment['class'] = variable_get('sharebuttons_alignment');
    $node->content['sharebuttons'] = array(
      '#markup' => theme('sharebuttons-widget', array('sharebuttons' => $sharebuttons, 'alignment' => $alignment)),
      '#weight' => variable_get('sharebuttons_weight'),
    );
  }

}


/**
 * Implements hook_block_info().
 */
function sharebuttons_block_info() {

  $blocks['sharebuttons'] = array(
    'info' => 'Sharebuttons',
    'cache' => DRUPAL_NO_CACHE,
  );
  return $blocks;

}


/**
 * Implements hook_block_view().
 */
function sharebuttons_block_view($delta = '') {

  // Return if no plugin is enabled.
  if (!sharebuttons_plugins_enabled()) {
    return;
  }

  // Output block.
  if (variable_get('sharebuttons_block')) {
    $url = url(current_path(), array('absolute' => TRUE));
    $sharebuttons = sharebuttons_widget($url);
    $alignment['class'] = variable_get('sharebuttons_alignment');

    $block['subject'] = 'Sharebuttons';
    $block['content'] = array(
      '#markup' => theme('sharebuttons-widget', array('sharebuttons' => $sharebuttons, 'alignment' => $alignment)),
    );
    return $block;
  }
}


/**
 * Check if at least 1 provider is enabled.
 */
function sharebuttons_plugins_enabled() {

  $plugins = sharebuttons_plugins();
  foreach ($plugins as $name => $info) {
    if (variable_get('sharebuttons_' . $name . '_enabled')) {
      return 1;
    }
  }

}


/**
 * Scan for plugins and return a sorted list.
 */
function sharebuttons_plugins() {

  $dir = drupal_get_path('module', 'sharebuttons') . '/plugins';
  $mask = '/(\.\.?|inc)$/';
  $options = array('key' => 'name');
  $plugins = file_scan_directory($dir, $mask, $options, 0);

  // Give indexes to plugins which are set /admin/config/services/sharebuttons.
  foreach ($plugins as $name => $info) {
    $index = variable_get('sharebuttons_' . $name . '_order');
    $items[$name] = $index;
  }

  // Sort to maintain the order that is set by admin.
  asort($items);
  return $items;

}
